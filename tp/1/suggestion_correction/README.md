# Suggestion de correction : TP1 - Mise en jambes

# Sommaire
- [B1 Réseau 2019 - TP1](#b1-réseau-2019---tp1)
- [TP1 - Mise en jambes](#tp1---mise-en-jambes)
- [Sommaire](#sommaire)
- [Déroulement et rendu du TP](#déroulement-et-rendu-du-tp)
- [I. Exploration locale en solo](#i-exploration-locale-en-solo)
  - [1. Affichage d'informations sur la pile TCP/IP locale](#1-affichage-dinformations-sur-la-pile-tcpip-locale)
    - [En ligne de commande](#en-ligne-de-commande)
    - [En graphique (GUI : Graphical User Interface)](#en-graphique-gui--graphical-user-interface)
    - [Questions](#questions)
  - [2. Modifications des informations](#2-modifications-des-informations)
    - [A. Modification d'adresse IP (part 1)](#a-modification-dadresse-ip-part-1)
    - [B. Table ARP](#b-table-arp)
    - [C. `nmap`](#c-nmap)
    - [D. Modification d'adresse IP (part 2)](#d-modification-dadresse-ip-part-2)
- [II. Exploration locale en duo](#ii-exploration-locale-en-duo)
  - [1. Prérequis](#1-prérequis)
  - [2. Câblage](#2-câblage)
  - [Création du réseau (oupa)](#création-du-réseau-oupa)
  - [3. Modification d'adresse IP](#3-modification-dadresse-ip)
  - [4. Utilisation d'un des deux comme gateway](#4-utilisation-dun-des-deux-comme-gateway)
  - [5. Petit chat privé](#5-petit-chat-privé)
  - [6. Firewall](#6-firewall)
- [III. Manipulations d'autres outils/protocoles côté client](#iii-manipulations-dautres-outilsprotocoles-côté-client)
  - [1. DHCP](#1-dhcp)
  - [2. DNS](#2-dns)
- [IV. Wireshark](#iv-wireshark)
- [Bilan](#bilan)

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

En utilisant la ligne de commande (CLI) de votre OS :

**🌞 Affichez les infos des cartes réseau de votre PC**

```bash
[it4@nowhere ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s31f6: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether c8:5b:76:1b:3c:81 brd ff:ff:ff:ff:ff:ff
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether e4:b3:18:48:36:68 brd ff:ff:ff:ff:ff:ff
    inet 10.33.2.84/22 brd 10.33.3.255 scope global dynamic noprefixroute wlan0
       valid_lft 4680sec preferred_lft 4680sec
    inet6 fe80::8722:113b:341a:f7b/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```

| Nom interface | IP         | MAC               |
|---------------|------------|-------------------|
| `enp0s31f6`   | Aucune     | c8:5b:76:1b:3c:81 |
| `wlan0`       | 10.33.2.84 | e4:b3:18:48:36:68 |

**🌞 Affichez votre gateway**

```bash
[it4@nowhere ~]$ ip r s
default via 10.33.3.253 dev wlan0 proto dhcp metric 600 
10.33.0.0/22 dev wlan0 proto kernel scope link src 10.33.2.84 metric 600 
```

La passerelle est 10.33.3.253.

### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS :  

[](./pics/ip_infos.png)

### Questions

La gateway du réseau d'YNOV nous permet de sortir du LAN d'YNOV vers d'autres réseau, comme par exemple internet.

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

[](./pics/edit_ip_gui.png)

On peut perdre notre accès internet dans le cas où l'IP choisie est déjà utilisée par quelqu'un d'autre sur le réseau.

### B. Table ARP


Table ARP :

```bash
[it4@nowhere ~]$ ip n s
10.33.2.193 dev wlan0 lladdr e0:2b:e9:7d:a6:c2 STALE
10.33.3.33 dev wlan0 lladdr c8:58:c0:63:5a:92 STALE
10.33.3.254 dev wlan0 lladdr 00:0e:c4:cd:74:f5 STALE
10.33.3.253 dev wlan0 lladdr 00:12:00:40:4c:bf REACHABLE
10.33.3.103 dev wlan0 lladdr 84:1b:77:96:85:02 STALE
```

L'adresse de passerelle est 10.33.3.253 (on le sait grâce à la table de routage affichée plus haut, avec la commande `ip r s`).  
On repère alors cette ligne dans la table ARP :
```bash
10.33.3.253 dev wlan0 lladdr 00:12:00:40:4c:bf REACHABLE
```

On peut ainsi conclure que la MAC de la passerelle 10.33.3.253 est 00:12:00:40:4c:bf.

---

Pour les plus curieux et avides de savoir dans `bash`, voici la boucle que j'ai utilisé pour ping des IPs random. Elle envoie en parallèle 10 pings.
```bash
for i in $(seq 1 10); do ( ( ping -c 1 10.33.3.$i ) & ) ; done
```

La table ARP après les 10 pings random :
```bash

[it4@nowhere ~]$ ip n s
10.33.3.253 dev wlan0 lladdr 00:12:00:40:4c:bf REACHABLE
10.33.3.103 dev wlan0 lladdr 84:1b:77:96:85:02 STALE
10.33.3.10 dev wlan0 lladdr a4:83:e7:59:ae:c2 REACHABLE
10.33.3.6 dev wlan0  FAILED
10.33.3.2 dev wlan0  FAILED
10.33.3.33 dev wlan0 lladdr c8:58:c0:63:5a:92 STALE
10.33.3.9 dev wlan0 lladdr 3c:22:fb:0a:2c:f6 REACHABLE
10.33.3.1 dev wlan0  FAILED
10.33.3.5 dev wlan0  FAILED
10.33.3.8 dev wlan0  FAILED
10.33.3.4 dev wlan0 lladdr 74:d8:3e:3e:99:41 REACHABLE
10.33.3.254 dev wlan0 lladdr 00:0e:c4:cd:74:f5 STALE
10.33.2.193 dev wlan0 lladdr e0:2b:e9:7d:a6:c2 STALE
10.33.3.7 dev wlan0  FAILED
10.33.3.3 dev wlan0  FAILED
```

Les adresses MAC qu'on vient de récupérer grâce à nos pings sont les suivantes :
```bash
[it4@nowhere ~]$ ip n s
10.33.3.10 dev wlan0 lladdr a4:83:e7:59:ae:c2 REACHABLE
10.33.3.6 dev wlan0  FAILED
10.33.3.2 dev wlan0  FAILED
10.33.3.9 dev wlan0 lladdr 3c:22:fb:0a:2c:f6 REACHABLE
10.33.3.1 dev wlan0  FAILED
10.33.3.5 dev wlan0  FAILED
10.33.3.8 dev wlan0  FAILED
10.33.3.4 dev wlan0 lladdr 74:d8:3e:3e:99:41 REACHABLE
10.33.3.7 dev wlan0  FAILED
10.33.3.3 dev wlan0  FAILED
```

### C. `nmap`


🌞**Utilisez `nmap` pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre**
- réinitialiser votre conf réseau (reprenez une IP automatique, en vous déconnectant/reconnectant au réseau par exemple)
- lancez **un scan de ping** sur le réseau YNOV
- affichez votre table ARP

```bash
[it4@nowhere ~]$ sudo nmap -sn 10.33.0.0/22
[...]

Nmap scan report for 10.33.3.244
Host is up (0.023s latency).
MAC Address: 50:EB:71:D6:6B:F9 (Intel Corporate)
Nmap scan report for 10.33.3.246
Host is up (0.070s latency).
MAC Address: 4C:79:6E:D6:74:7D (Intel Corporate)
Nmap scan report for 10.33.3.252
Host is up (0.0023s latency).
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for 10.33.3.253
Host is up (0.0084s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.0047s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.2.84
Host is up.
Nmap done: 1024 IP addresses (202 hosts up) scanned in 31.89 seconds
```

### D. Modification d'adresse IP (part 2)

- 🌞 Modifiez de nouveau votre adresse IP vers une adresse IP que vous savez libre grâce à `nmap`
  - utilisez un ping scan sur le réseau YNOV
  - montrez moi la commande `nmap` et son résultat
  - configurez correctement votre gateway pour avoir accès à d'autres réseaux (utilisez toujours la gateway d'YNOV)
  - prouvez en une suite de commande que vous avez bien l'IP choisie, que votre passerelle est bien définie, et que vous avez un accès internet

Grâce au résultat du `nmap` lancé plus tôt, on a notamment les lignes :

```bash
[...]
Nmap scan report for 10.33.3.244
Host is up (0.023s latency).
MAC Address: 50:EB:71:D6:6B:F9 (Intel Corporate)
Nmap scan report for 10.33.3.246
Host is up (0.070s latency).
[...]
```

On en conclut que l'ip 10.33.3.245 est probablement libre.

```bash
# vérification de l'IP
[it4@nowhere ~]$ ip a
[...]
3: wlan0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
    link/ether e4:b3:18:48:36:68 brd ff:ff:ff:ff:ff:ff
    inet 10.33.3.245/24 scope global wlan0
       valid_lft forever preferred_lft forever
    inet6 fe80::8722:113b:341a:f7b/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

# vérification de la passerelle
[it4@nowhere ~]$ ip r s
default via 10.33.3.253 dev wlan0 
10.33.3.0/24 dev wlan0 proto kernel scope link src 10.33.3.245 

# vérification de l'accès internet
[it4@nowhere ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=18.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=21.8 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 18.698/20.229/21.761/1.531 ms
```

# II. Exploration locale en duo

## 5. Petit chat privé

Sur la machine "serveur" :

```bash
[it4@nowhere ~]$ nc -l 127.0.0.1 -p 8888
mi
```

Sur le "client" :
```bash
[it4@nowhere ~]$ nc 127.0.0.1 8888
mi
```

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

Sous Linux, j'ai obtenu les infos DHCP avec :
```bash
[it4@nowhere ~]$ nmcli con show WiFi@YNOV | grep -i DHCP
[...]
DHCP4.OPTION[1]:                        dhcp_lease_time = 5515
DHCP4.OPTION[2]:                        dhcp_server_identifier = 10.33.3.254
DHCP4.OPTION[3]:                        domain_name = auvence.co
DHCP4.OPTION[4]:                        domain_name_servers = 10.33.10.2 10.33.10.148 10.33.10.155
DHCP4.OPTION[5]:                        expiry = 1632396159
[...]
```

Le serveur DHCP se trouve en 10.33.3.254.

La durée du bail est de 5515 secondes (1h30 à peu près).  

Ce bail expirera à la seconde 1632396159 depuis le temps EPOCH.
- il est actuellement "Thu Sep 23 11:59:16 AM CEST 2021"
- 1632396159 en temps EPOCH correspond à "Thursday, September 23, 2021 1:22:39 PM" 

Mon bail DHCP va expirer d'ici 1 heure et 22 minutes à peu près :)


## 2. DNS

- 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```bash
[it4@nowhere ~]$ cat /etc/resolv.conf 
# Generated by NetworkManager
search auvence.co nowhere.it4
nameserver 10.33.10.2
nameserver 10.33.10.148
nameserver 10.33.10.155
```

Requête DNS vers google.com, on apprend que "google.com" correspond à l'IP 216.58.198.206

```bash
[it4@nowhere ~]$ dig google.com

; <<>> DiG 9.16.20 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 9243
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		217	IN	A	216.58.198.206

;; Query time: 3 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Thu Sep 23 12:00:49 CEST 2021
;; MSG SIZE  rcvd: 57
```

Requête DNS vers ynov.com, on apprend que "ynov.com" correspond à l'IP 92.243.16.143

```bash
[it4@nowhere ~]$ dig ynov.com

; <<>> DiG 9.16.20 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 131
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;ynov.com.			IN	A

;; ANSWER SECTION:
ynov.com.		7023	IN	A	92.243.16.143

;; Query time: 6 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Thu Sep 23 12:00:51 CEST 2021
;; MSG SIZE  rcvd: 53
```

Requête DNS inverse (reverse lookup) vers 78.74.21.21, on apprend que cette IP correspond au nom host-78-74-21-21.homerun.telia.com.

```bash
[it4@nowhere ~]$ dig -x 78.74.21.21

; <<>> DiG 9.16.20 <<>> -x 78.74.21.21
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62425
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;21.21.74.78.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
21.21.74.78.in-addr.arpa. 3600	IN	PTR	host-78-74-21-21.homerun.telia.com.

;; Query time: 146 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Thu Sep 23 12:00:59 CEST 2021
;; MSG SIZE  rcvd: 101
```

Requête DNS inverse (reverse lookup) vers 92.146.54.88, on apprend que cette IP correspond au nom apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr.

```bash
[it4@nowhere ~]$ dig -x 92.146.54.88

; <<>> DiG 9.16.20 <<>> -x 92.146.54.88
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 45134
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4000
;; QUESTION SECTION:
;88.54.146.92.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
88.54.146.92.in-addr.arpa. 3600	IN	PTR	apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr.

;; Query time: 246 msec
;; SERVER: 10.33.10.2#53(10.33.10.2)
;; WHEN: Thu Sep 23 12:01:09 CEST 2021
;; MSG SIZE  rcvd: 113
```

# IV. Wireshark

> Pour la correction, j'ai effectué les tests en me connectant à moi-même (je n'avais pas de deuxième PC :( )

- un `ping` entre vous et la passerelle

En voici 3 (3 pings et leurs 3 pongs retour).

[](./pics/ping_gw.png)


- un `netcat` entre vous et votre mate, branché en RJ45

On filtre avec `port == 8888` pour n'avoir que notre netcat sur le port 8888/tcp.

[](./pics/nc.png)

- une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.

On voit ici l'adresse IP de destination `1.1.1.1` ainsi que la question posée : on souhaite obtenir l'adresse IP associée à `google.com`.

[](./pics/dns.png)
