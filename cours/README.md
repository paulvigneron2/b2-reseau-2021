# Cours Linux B2

Cours :

- [IP](./ip/README.md)
- [ARP](./arp/README.md)
- [Routage](./routage/README.md)

Mémos :

- [Mémo réseau Rocky Linux](./memo/rocky_network.md)
- [Etapes installation de la VM patron Rocky Linux](./memo/install_vm.md)

Lexique :

- [Lexique](./lexique/README.md)